import { Button } from 'react-bootstrap';
import React from 'react';

function Boton(props) {
    return (
        <Button variant={`${props.color}`} type="submit" onClick={props.onclick}>{props.text}</Button>
    );
  }
  
  export default Boton;  