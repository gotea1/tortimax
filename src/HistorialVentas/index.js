import React from "react";
import Logo from '../iconos/tortimax96.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'; 
import { faSignOut, faHistory } from '@fortawesome/free-solid-svg-icons';
import { Link } from "react-router-dom"; //agregué para la ruta

//para lalista e item
import { ListaVenta } from '../Lista';
import { ItemVenta } from '../Item';

function HistorialVentas(props) {

    //array de ventas para ejemplo
    const ventasArray = [
        {
          numerodeventa: 1,
          fecha: '10-12-22',
          monto: 100,
        },
        {
          numerodeventa: 2,
          fecha: '11-12-22',
          monto: 200,
        },
        {
          numerodeventa: 3,
          fecha: '12-12-22',
          monto: 300,
        },
        {
          numerodeventa: 4,
          fecha: '28-3-22',
          monto: 150,
        },
      ]

      //state lista
    const [ventas, setVentas] = React.useState(ventasArray);
    return(
    <div className="contenedor tarjetaMovil">
      <div className="contenedorMenu">
          <img src={Logo} alt="logo" className="logo"/>

          <span className='botonSalir'>
            <Link to={`/`} className="btn btn-success">
              Cerrar Sesión&nbsp;
              <FontAwesomeIcon
                size="1x"
                icon={faSignOut}
              /></Link>
          </span>
          <span className='iconoSalir'>
            <Link to={`/`} className="btn btn-success">
              <FontAwesomeIcon
                size="1x"
                icon={faSignOut}
              /></Link>
          </span>    
      </div>

    <div className="contenedorCuerpo">
    <h2 className="titulo">Historial de ventas</h2>
        <div className="tarjeta">
        {/* Componente Lista e Item  Tabla ventas */}
            <ListaVenta className="lista">
                {ventas.map(venta => (
                    <ItemVenta
                        primero={venta.numerodeventa}
                        segundo={venta.fecha}
                        tercero={venta.monto}
                        />
                ))
                }
            </ListaVenta>
        </div>
        
        <span className="botonReturn">
          <Link to={`/InicioDueno`} className="btn btn-success">
            Regresar&nbsp;
          <FontAwesomeIcon
            size="1x"
            icon={faHistory}
          /></Link>
        </span>
        
        <span className="iconoReturn">
          <Link to={`/InicioDueno`} className="btn btn-success">
          <FontAwesomeIcon
            size="1x"
            icon={faHistory}
          /></Link>
        </span>
    </div>
  </div>
  
    );
};

export {HistorialVentas};