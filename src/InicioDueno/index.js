import React from 'react';
import logo from '../iconos/tortimax96.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOut, faBasketShopping, faHandHoldingDollar } from '@fortawesome/free-solid-svg-icons';
import { Link } from "react-router-dom";


function InicioDueno (props) {
  const name = "Tadeo";
    return(
      <div className="contenedor tarjetaMovil">
      <div className="contenedorMenu">
          <img src={logo} alt="logo" className="logo"/>

          <span className='botonSalir'>
            <Link to={`/`} className="btn btn-success">
              Cerrar Sesión&nbsp;
              <FontAwesomeIcon
                size="1x"
                icon={faSignOut}
              /></Link>
          </span>
          <span className='iconoSalir'>
            <Link to={`/`} className="btn btn-success">
              <FontAwesomeIcon
                size="1x"
                icon={faSignOut}
              /></Link>
          </span>    
      </div>

    <div className="contenedorCuerpo">
      <h2 className='titulo'>Bienvenido {name}</h2>
      <div className="tarjeta iconosDueno">

              <span className="botonDueno">
                <Link to={`/historialpedidos`} className="btn btn-success">
                  <FontAwesomeIcon
                        icon={faBasketShopping}
                        color='#fff'
                        size='6x'/> <br /> Pedidos
                </Link>
              </span>

              <span className="botonDueno">
                <Link to={`/historialventas`} className="btn btn-info">
                  <FontAwesomeIcon
                        icon={faHandHoldingDollar}
                        color='#fff'
                        size='6x'/> <br /> Ventas
                </Link>
              </span>
      </div>
    </div>
  </div>
    );
}

export {InicioDueno};