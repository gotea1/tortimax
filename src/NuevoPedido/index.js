import React from "react";
import Logo from '../iconos/tortimax96.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignIn, faBasketShopping } from '@fortawesome/free-solid-svg-icons';
import { Link } from "react-router-dom";
import {
    InputText,
    InputPhone,
    InputSelectMetodoPago,
    InputSelectTipoCompra,
    InputSelectProducto,
    InputNumber,
} from '../Input';
import Boton from "../Boton";

function NuevoPedido() {
    return (
        <div className="Contenedor tarjetaMovil">
            <div className="contenedorMenu">
                <img src={Logo} alt="logo" className="logo" />

                <span className='botonSalir'>
                    <Link to={`/login`} className="btn btn-success">
                        Iniciar Sesión&nbsp;
                        <FontAwesomeIcon
                            size="1x"
                            icon={faSignIn}
                        /></Link>
                </span>
                <span className='iconoSalir'>
                    <Link to={`/login`} className="btn btn-success">
                        <FontAwesomeIcon
                            size="1x"
                            icon={faSignIn}
                        /></Link>
                </span>
            </div>

            <div className="Contenido contenedorCuerpo">
                <h2 className='titulo'>Nuevo pedido</h2>
                <div className="tarjeta container">

                    <form className="row g-3">
                        <div className="col-md-4 inputColumna">
                            <label htmlFor="domicilio" className="form-label nombreInput">Domicilio</label>
                            <InputText placeholder={"Ingrese su domicilio: Ej. Av. 5 de febrero #111"} id="domicilio" />
                        </div>
                        <div className="col-md-4 inputColumna">
                            <label htmlFor="nombre" className="form-label nombreInput">Nombre</label>
                            <InputText placeholder={"Escriba su nombre"} id="nombre" />
                        </div>
                        <div className="col-md-4 inputColumna">
                            <label htmlFor="contacto" className="form-label nombreInput">Número de contacto</label>
                            <InputPhone placeholder={"Escriba un número de contacto"} id="contacto" />
                        </div>
                        <div className="col-md-4 inputColumna">
                            <label htmlFor="producto" className="form-label nombreInput">Producto</label>
                            <InputSelectProducto id="producto" />
                        </div>
                        <div className="col-md-4 inputColumna">
                            <label htmlFor="cantidad" className="form-label nombreInput">Cantidad</label>
                            <InputNumber placeholder={"Cantidad en kilogramos"} id="cantidad" />
                        </div>
                        <div className="col-md-4 inputColumna">
                            <label htmlFor="compra" className="form-label nombreInput">Tipo de compra</label>
                            <InputSelectTipoCompra id="compra" />
                        </div>
                        <div className="col-md-4">
                            <label htmlFor="pago" className="form-label nombreInput">Método de pago</label>
                            <InputSelectMetodoPago id="pago" />
                        </div>
                        {/* Aqui va el Button de forms o puedo jalar los datos con el span de abajo?*/}
                    </form>
                </div>

                <span className="botonReturn">
                    <Boton
                        onclick={() => {
                            if(window.confirm("La compra tendrá un costo de $150, ¿Confirmar compra?")){
                                alert("Pedido hecho con éxito");
                                window.location.reload(false);
                            }
                        }}
                        text={
                            <span>
                                Confirmar
                                <FontAwesomeIcon
                                    className="icon"
                                    size="1x"
                                    icon={faBasketShopping}
                                />
                            </span>
                        }
                        color="success"
                    />
                </span>
                <span className="iconoReturn">
                    <Boton
                        onclick={() => {
                            alert("Pedido hecho con éxito");
                            window.location.reload(false);
                        }}
                        text={
                            <span>
                                Confirmar
                                <FontAwesomeIcon
                                    className="icon"
                                    size="1x"
                                    icon={faBasketShopping}
                                />
                            </span>
                        }
                        color="success"
                    />
                </span>
            </div>
        </div>
    )
}

export { NuevoPedido };